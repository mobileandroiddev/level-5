package com.example.annabel.numbertrivia;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class NumberTriviaAdapter extends RecyclerView.Adapter<NumberTriviaAdapter.ViewHolder> {

    private List<NumberTrivia> numbers;

    public NumberTriviaAdapter(List<NumberTrivia> numbers) {

        this.numbers = numbers;
    }

    @Override
    public NumberTriviaAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.number_view, viewGroup, false);
        NumberTriviaAdapter.ViewHolder viewHolder = new NumberTriviaAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumberTriviaAdapter.ViewHolder viewHolder, int position) {
        NumberTrivia number = numbers.get(position);
        viewHolder.numberView.setText(Integer.toString(number.getNumber()));
        viewHolder.triviaView.setText(number.getText());
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public void updateList(List newNumbers) {
        this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView numberView;
        public TextView triviaView;

        public ViewHolder(View itemView){
            super(itemView);
            numberView = itemView.findViewById(R.id.numberView);
            triviaView = itemView.findViewById(R.id.triviaView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }

    }

}
