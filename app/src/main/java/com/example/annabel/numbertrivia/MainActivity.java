package com.example.annabel.numbertrivia;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class MainActivity extends AppCompatActivity {


    private List<NumberTrivia> numbers;
    private RecyclerView recyclerView;
    private NumberTriviaAdapter triviaAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        numbers = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        triviaAdapter = new NumberTriviaAdapter(numbers);
        recyclerView.setAdapter(triviaAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newNumberTrivia();
//                updateUI();
            }
        });
    }

    private void newNumberTrivia(){
        NumbersApiService service = NumbersApiService.retrofit.create(NumbersApiService.class);

        Call<NumberTrivia> call = service.getTrivia();
        call.enqueue(new Callback<NumberTrivia>() {

            @Override
            public void onResponse(Call<NumberTrivia> call, Response<NumberTrivia> response) {
                NumberTrivia numberTrivia = response.body();
                numbers.add(numberTrivia);
                updateUI();
            }

            @Override
            public void onFailure(Call<NumberTrivia> call, Throwable t) {
                Log.d("error",t.toString());
            }
        });



    }

    private void updateUI() {
        triviaAdapter.updateList(numbers);
    }


    public interface NumbersApiService {

        String BASE_URL = "http://numbersapi.com/";

         //Create a retrofit client.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        @GET("/random/trivia?json")

        Call<NumberTrivia> getTrivia();

    }




}
